# Level 01 Instructions:

## Read /etc/passwd
```
$ cat /etc/passwd
<trimmed>
flag00:x:3000:3000::/home/flag/flag00:/bin/bash
flag01:42hDRfypTqqnw:3001:3001::/home/flag/flag01:/bin/bash
flag02:x:3002:3002::/home/flag/flag02:/bin/bash
<trimmed>
```

## Decrypt password for flag01 from host system using hashcat and rockyou.txt
```
$ hashcat -m 1500 -a 0 --force -D 1 -o answer --quiet 42hDRfypTqqnw ~/Downloads/rockyou.txt
```

## Read output
```
$ cat answer
42hDRfypTqqnw:abcdefg
```

## Log into flag01
```
$ su flag01
Password: abcdefg
```

## Get flag
```
$ getflag
Check flag.Here is your token : f2av5il02puano7naaf6adaaf
```
