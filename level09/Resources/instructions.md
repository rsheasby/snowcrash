# Level 09 instructions:

## Check home directory:
```
total 12
-rwsr-sr-x 1 flag09 level09 7640 Mar  5  2016 level09
----r--r-- 1 flag09 level09   26 Mar  5  2016 token
```

This time we can access the token file, but it's enciphered, presumably using the level09 executable.

## Try run the executable:
```
$ ./level09
You need to provied only one arg.
$ ./level09 aaaaaaaaaa
abcdefghij
```

It seems that it's using a variation of a caesar cipher, where it's shifting by one character per string position.

## Make simple program to reverse string:
<Find program from resources directory>

## Decipher token file using our program:
```
$ export CONTENT=`cat token`
$ /tmp/shift $CONTENT
f3iji1ju5yuevaus41q1afiuq
``` 

# Get flag:
```
$ su flag09
Password: f3iji1ju5yuevaus41q1afiuq
Don't forget to launch getflag !
$ getflag
Check flag.Here is your token : s5cAJpM8ev6XHw998pRWG728z
```
