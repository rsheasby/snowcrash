# Level 13 instructions:

## Check home directory:
```
$ ls -l
total 8
-rwsr-sr-x 1 flag13 level13 7303 Aug 30  2015 level13
```

## Try run level13:
```
$ ./level13
UID 2013 started us but we we expect 4242
```

We can only run the program if it thinks we have the UID 4242. Let's spoof the UID using LD_PRELOAD.

## Make spoofed getuid library:
<Find source code for program in Resources

```
$ cd /tmp
$ gcc -shared getuid.c -o getuid.so
```

## Run program from gdb to disable setuid bit:
```
$ LD_PRELOAD=/tmp/getuid.so gdb ./level13
GNU gdb (Ubuntu/Linaro 7.4-2012.04-0ubuntu2.1) 7.4-2012.04
Copyright (C) 2012 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "i686-linux-gnu".
For bug reporting instructions, please see:
<http://bugs.launchpad.net/gdb-linaro/>...
Reading symbols from /home/user/level13/level13...(no debugging symbols found)...done.
(gdb) run
Starting program: /home/user/level13/level13
your token is 2A31L79asukciNyi8uppkEuSx
[Inferior 1 (process 3150) exited with code 050]
(gdb) quit
```
