# Level 00 Instructions:

## Find files owned by flag00:
```
$ find / -user flag00 2> /dev/null
/usr/sbin/john
/rofs/usr/sbin/john
```

## Open file:
```
$ cat /usr/sbin/john
cdiiddwpgswtgt
```

## Try caesar ciphers:
```
$ OUTPUT=cdiiddwpgswtgt
$ for i in {1..26}
$ do
$   OUTPUT=$(echo $OUTPUT | tr "[a-z]" "[b-za]")
$   echo $OUTPUT
$ done
dejjeexqhtxuhu
efkkffyriuyviv
fgllggzsjvzwjw
ghmmhhatkwaxkx
hinniibulxbyly
ijoojjcvmyczmz
jkppkkdwnzdana
klqqllexoaebob
lmrrmmfypbfcpc
mnssnngzqcgdqd
nottoohardhere
opuuppibseifsf
pqvvqqjctfjgtg
qrwwrrkdugkhuh
rsxxsslevhlivi
styyttmfwimjwj
tuzzuungxjnkxk
uvaavvohykolyl
vwbbwwpizlpmzm
wxccxxqjamqnan
xyddyyrkbnrobo
yzeezzslcospcp
zaffaatmdptqdq
abggbbunequrer
bchhccvofrvsfs
cdiiddwpgswtgt
```

## Log in to flag00
```
$ su flag00
Password: nottoohardhere
Don't forget to launch getflag !
$ getflag
Check flag.Here is your token : x24ti5gi3x0ol2eh4esiuxias
```
