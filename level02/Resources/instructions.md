# Level 02 instructions:

## Check home directory:
```
$ ls
level02.pcap
```

## Download home directory from host machine:
```
$ scp -P 4242 level02@<ip-address>:~/level02.pcap .
           _____                      _____               _
          / ____|                    / ____|             | |
         | (___  _ __   _____      _| |     _ __ __ _ ___| |__
          \___ \| '_ \ / _ \ \ /\ / / |    | '__/ _` / __| '_ \
          ____) | | | | (_) \ V  V /| |____| | | (_| \__ \ | | |
         |_____/|_| |_|\___/ \_/\_/  \_____|_|  \__,_|___/_| |_|

  Good luck & Have fun

          <ip-address>
level02@<ip-address>'s password: f2av5il02puano7naaf6adaaf
level02.pcap
```

## Inspect pcap file using Wireshark:
Open Wireshark
File>>Open>>Navigate to level02.pcap
Right click first packet>>Follow>>TCP Stream
Change "Show and save data as" to UTF-8
The result is:
```
..%..%..&..... ..#..'..$..&..... ..#..'..$.. .....#.....'........... .38400,38400....#.SodaCan:0....'..DISPLAY.SodaCan:0......xterm.........."........!........"..".....b........b....	B.
..............................1.......!.."......"......!..........."........"..".............	..
.....................
Linux 2.6.38-8-generic-pae (::ffff:10.1.1.2) (pts/10)

..wwwbugs login: l.le.ev.ve.el.lX.X
..
Password: ft_wandr...NDRel.L0L
.
..
Login incorrect
wwwbugs login: 
```

## Login to flag02 using password from packet capture:
```
$ su flag02
Password: ft_wandr...NDRel.L0L
Don't forget to launch getflag !
```

## Get flag
```
$ getflag
Check flag.Here is your token : kooda2puivaav1idi4f57q8iq
```
